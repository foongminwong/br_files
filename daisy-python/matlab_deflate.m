syms I Pi;
I=1i;
syms x y z;
X = [ x y z];
deg = [ 6 4 5 5];
f1=(x^2-y^3)^2-(z^2-y^2)^3;
f_1_1 = (4*x*(x^2 - y^3))/3;
f_1_2 = -2*y*(2*y^2*z^2 + x^2*y - 2*y^4 - z^4);
f_1_3 = -2*z*(y^2 - z^2)^2;

F = [ f1 f_1_1 f_1_2 f_1_3];
J = jacobian(F,X);
R = nchoosek(1:4,2);
C = nchoosek(1:3,2);
r = size(R,1);
c = size(C,1);
count = 0;
OUT = fopen('deflation_polynomials','w');
for j = 1:r
  for k = 1:c
    A = simplify(det(J(R(j,:),C(k,:))));
    if A ~= 0
      count = count + 1;
		curr_eqn = char(2*A/prod(deg(R(j,:))));%
		i_locations = regexp(curr_eqn,'[\W\s]i[\W\s]');
		curr_eqn(i_locations+1) = 'I';
      fprintf(OUT, 'f_2_%d = %s;\n', count, curr_eqn);
    end;
  end;
end;
fclose(OUT);
OUT = fopen('deflation_polynomials_declaration','w');
fprintf(OUT, 'function ');
for j = 1:count
  fprintf(OUT, 'f_2_%d', j);
  if j == count    fprintf(OUT, ';\n');
  else    fprintf(OUT, ',');
  end;
end;

exit
