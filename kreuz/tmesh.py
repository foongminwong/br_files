import bertini_real
#bertini_real.data.gather()
#bertini_real.tmesh.raw()
#bertini_real.tmesh.smooth()
#bertini_real.tmesh.solidify()
#bertini_real.tmesh.test()
#bertini_real.tmesh.solidify()
#bertini_real.tmesh.test()

bertini_real.data.gather()

bertini_real.glumpyplotter.plot_surface_samples()
#bertini_real.glumpyplotter.plot_critical_curve()

bertini_real.tmesh.stl_raw()
bertini_real.tmesh.stl_smooth()
bertini_real.tmesh.solidify_raw()
bertini_real.tmesh.solidify_smooth()
