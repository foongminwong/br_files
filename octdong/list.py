def let_user_pick(options):
    print("Please choose:")
    for idx, element in enumerate(options):
        print("{}) {}".format(idx+1,element))
    i = input("Enter number: ")
    try:
        if 0 < int(i) <= len(options):
            print(int(i))
            return int(i)
    except:
        pass
    return None



options = ["Option 1", "Option 2", "Option 3"]
let_user_pick(options)
