import numpy as np
from numpy import array
import math
import itertools
from itertools import combinations
import sympy as sp
from sympy import *
import scipy as sci
import pickle

def my_nchoosek( r, n ):
	cnt = 0
	for i in combinations(range(1,r+1),n):
		if cnt == 0:
			R = Matrix([i])
		else:
			R_i = Matrix([i])
			R = R.row_insert(cnt,R_i)
		cnt = cnt + 1
	return R;

from numpy import pi
from sympy import sin, cos, Matrix, simplify, var
from sympy.abc import I, pi
I=1j

# variables
var('x, y, z')
X = Matrix([x, y, z])

# Degrees of the equations
deg = np.array([6, 5, 5, 3])
f1=Matrix([(y**2+z**2-1)**2+(x**2+y**2-1)**3])

f_1_1 =Matrix([ x*(x**2 + y**2 - 1)**2/3])
f_1_2 =Matrix([ y*(2*y**2 + 2*z**2 + 3*(x**2 + y**2 - 1)**2 - 2)/9])
f_1_3 =Matrix([ 2*z*(y**2 + z**2 - 1)/9])

# Matrix of the functions

F = sp.Matrix(f1)
F = F.col_insert(1,f_1_1)
F = F.col_insert(2, f_1_2)
F = F.col_insert(3, f_1_3)

J = F.jacobian(X)

# getting combinations of the outputs
R = my_nchoosek(4,2);
C = my_nchoosek(3,2)

# getting the sizes of R and C
r_t = Matrix([R.shape])
c_t = Matrix([C.shape])

# Turning the sizes into an array
rv = np.array([r_t])
cv = np.array([c_t])

# Getting the first element in the array of sizes
r = rv.item(0)
c = cv.item(0)
count = 0;

# Opening the first output file
fo = open("deflation_polynomials","w")
for j in range(1,r+1):
	for k in range(1,c+1):
		temp_R = np.array(R.row(j-1))
		temp_C = np.array(C.row(k-1))

# Calculating whether temp_R is a 1x1 Matrix or not
		tR_s = np.array([temp_R.shape])

# Choosing the rows in the determinant
		if tR_s.item(0)*tR_s.item(1) > 1:
			myJ_r = Matrix([J.row(temp_R.item(0)-1),J.row(temp_R.item(1)-1)])
		else:
			myJ_r = Matrix([J.row(temp_R.item(0)-1)])

# Calculating whether temp_C is singular or not
		tC_s = np.array([temp_C.shape])

# Choosing the columns of the determinant
		if tC_s.item(0)*tC_s.item(1) > 1:
			myA_1a = np.array(myJ_r.col(temp_C.item(0)-1))
			myA_1b = np.array(myJ_r.col(temp_C.item(1)-1))
			myA_1 = np.array([[myA_1a.item(0),myA_1b.item(0)],[myA_1a.item(1),myA_1b.item(1)]])
		else:
			myA_1 = np.array([myJ_r.col(temp_C.item(0)-1)])
		myA = Matrix(myA_1)
		mydet = myA.det()
		A = simplify(mydet)
		if A != 0:
			count = count + 1
			tx = temp_R.item(0)-1
			if temp_R.size > 1:
				ty = temp_R.item(1)-1
			else:
				ty = 0
			mydeg = np.array([deg.item(tx),deg.item(ty)])
			myprod = np.multiply(mydeg.item(0),mydeg.item(1))
			myfunc = 2*A/myprod
			myfunc2 = simplify(myfunc)
			curr_eqn = str(myfunc)
			tf_i = curr_eqn.find("1j")
			if tf_i != -1:
				curr_eqn = str.replace("1j","I")
			func_ida = "f_2_%i = " % count
			func_idb = str(curr_eqn)
			func_id = func_ida+func_idb+";\n"
			fo.write(func_id.replace("**","^"))
fo.close()


# Opening the second output file
fo = open("deflation_polynomials_declaration","w")
mystr4 = ""
for j in range(1,count+1):
	mystr2a = "f_2_%i" % (j)
	if j == count:
		mystr2b = ";\n"
	else:
		mystr2b = ", "
	mystr3 = "%s%s" % (mystr2a,mystr2b)
	mystr4 = "%s%s" % (mystr4,mystr3)


fo.write("function ")
fo.write(mystr4)
fo.write("\n")
fo.close()

